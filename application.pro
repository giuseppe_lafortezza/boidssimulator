#
# include widgets stuff
#


win32 {
    BUILD_TARGET = _win32
}


linux {
    BUILD_TARGET = _linux
}


CONFIG(debug, debug|release) {
    BUILD_MODE = _debug
}


CONFIG(release, debug|release) {
    BUILD_MODE = _release
}


APPLICATION_NAME = NIS
APPLICATION_SRC  = src
APPLICATION_INC  = include

TARGET   = $${APPLICATION_NAME}

TEMPLATE = app

CONFIG  -= debug_and_release
CONFIG  -= debug_and_release_target
CONFIG  += c++11
CONFIG  += c++14
CONFIG  += qt
CONFIG  += console
CONFIG  += depend_includepath

QT += core \
      gui \
      widgets

INCLUDEPATH += $${APPLICATION_INC}

HEADERS += $${APPLICATION_INC}/MyCanvas.h \
		$${APPLICATION_INC}/QSFMLCanvas.h \

SOURCES += 	$${APPLICATION_SRC}/MyCanvas.cpp \
		$${APPLICATION_SRC}/QSFMLCanvas.cpp \
			$${APPLICATION_SRC}/main.cpp


DEFINES += QT_DEPRECATED_WARNINGS

LIBS        += -lsfml-graphics -lsfml-window -lsfml-system

win32 {
    DEFINES += USE_THREADS_CPP11
}


linux {
    QMAKE_CXXFLAGS += -Weffc++ -pipe -Wshadow -Wextra
    QMAKE_CXXFLAGS_DEBUG += -g3
}


QMAKE_CXXFLAGS_RELEASE += -O2


BUILD_DIR   = ../build$${BUILD_TARGET}$${BUILD_MODE}
OBJECTS_DIR = $${BUILD_DIR}/obj
UI_DIR      = $${BUILD_DIR}/ui
MOC_DIR     = $${BUILD_DIR}/moc
RCC_DIR     = $${BUILD_DIR}/rcc


DESTDIR     = ../bin
